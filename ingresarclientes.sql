\c tarjetadecredito

INSERT INTO cliente VALUES(1, 'Juan', 'Gregorio', 'Rivadavia 2131', '11-3576-0584');
INSERT INTO cliente VALUES(2, 'Carlos', 'Pereyra', 'José Ingenieros 445', '11-6546-1183');
INSERT INTO cliente VALUES(3, 'José', 'Bauverde', 'Calamuchita 7245', '11-2345-6502');
INSERT INTO cliente VALUES(4, 'Joaquín', 'Vincente', 'Córdoba 2356', '11-5687-6124');
INSERT INTO cliente VALUES(5, 'Nicolás', 'Papini', 'Perú 234', '11-5476-1254');
INSERT INTO cliente VALUES(6, 'Jorge', 'Tifón', 'Brasil 1451', '11-8760-4326');
INSERT INTO cliente VALUES(7, 'Carmen Lucía', 'Estable', 'Magallanes 1557', '11-5271-6653');
INSERT INTO cliente VALUES(8, 'Rita', 'Plano', 'Sáenz Peña 2220', '11-3576-8880');
INSERT INTO cliente VALUES(9, 'Juan Pablo', 'Segundo', 'Vaticano 1234', '11-5421-3984');
INSERT INTO cliente VALUES(10, 'Alex', 'Ubago', 'Roca 523', '11-3244-2365');
INSERT INTO cliente VALUES(11, 'Giorno', 'Giovanna', 'De La Rúa 178', '11-6986-2976');
INSERT INTO cliente VALUES(12, 'Alfio', 'Argento', 'Flores 49', '11-5976-8524');
INSERT INTO cliente VALUES(13, 'Oriana', 'Waldorf', 'Uriburu 1145', '11-3592-1015');
INSERT INTO cliente VALUES(14, 'Alex', 'DeLarge', 'Plus Ultra 6271', '11-2987-6521');
INSERT INTO cliente VALUES(15, 'Mark', 'Renton', 'Escocia 3422', '11-9367-2095');
INSERT INTO cliente VALUES(16, 'Stanley', 'Kubrick', 'Gales 411', '11-2342-9876');
INSERT INTO cliente VALUES(17, 'Quentin', 'Tarantino', 'Gales 1232', '11-3259-8652');
INSERT INTO cliente VALUES(18, 'Luis', 'Archibals', 'Hamington 523', '11-3298-7523');
INSERT INTO cliente VALUES(19, 'Connor', 'McGregor', 'Las Vegas 75', '11-3532-6666');
INSERT INTO cliente VALUES(20, 'Ricardo', 'Fort', 'Miami 240', '11-5132-4586');
