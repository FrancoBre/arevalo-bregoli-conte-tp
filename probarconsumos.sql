\c tarjetadecredito

CREATE OR REPLACE FUNCTION probar_consumos() RETURNS VOID AS $$
DECLARE
	v RECORD;
	aux RECORD;
BEGIN

	FOR v IN (SELECT * FROM consumo)
	LOOP

		SELECT autorizacion_de_compra(v.nrotarjeta, v.codseguridad, v.nrocomercio, v.monto) INTO aux;

	END LOOP;

END;
$$ LANGUAGE PLPGSQL;
