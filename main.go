package main


//Imports
import (
	"database/sql"
	"encoding/json"
	"log"
	"fmt"
	"os"
	"strconv"
	_"github.com/lib/pq"
	bolt "github.com/coreos/bbolt"
)


//Declaraciones
type Cliente struct {
	NroCliente	int
	Nombre		string
	Apellido	string
	Domicilio	string
	Telefono	string
}

type Tarjeta struct {
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad string
	LimiteCompra float64
	Estado       string
}

type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal string
	Telefono     string
}

type Compra struct {
	NroOperacion int
	NroTarjeta   string
	NroComercio  int
	Fecha        string
	Monto        float64
	Pagado       bool
}

var sqldb *sql.DB
var boltdb *bolt.DB


//Funcion principal

func main() {
	menuPrincipal()
}


//Menu-----------------------------------------------------------------------------------------------------------------------------------------------------------------

func menuPrincipal(){
	opciones := []string{
		"SQL",
		"NoSQL",
		"Cerrar",
	}
	
	fmt.Println("\nMENU PRINCIPAL")
	fmt.Println("Por favor, seleccione una de las siguientes opciones\n")
	opcionSeleccionada := menu(opciones)
	
	switch opcionSeleccionada{
		case 1: 
			crearDbSQL()
			menuSQL()
		case 2:
			menuNoSQL()
		case 3: 
			if sqldb != nil {
				sqldb.Close()
			}
			if boltdb != nil {
				boltdb.Close()
			}
			os.Exit(0)	
		case -1:
			fmt.Println("\nLa opcion seleccionada es invalida\n")
			menuPrincipal();
	}
}

func menuSQL(){
	opciones := []string{
		"Crear tablas",
		"Insertar PKs y FKs",
		"Eliminar PKs y FKs",
		"Cargar cierres",
		"Llenar tablas",
		"Crear funciones",
		"Probar sistema",
		"Volver",
	}
	
	fmt.Println("\nMENU SQL")
	fmt.Println("Por favor, seleccione una de las siguientes opciones\n")
	
	opcionSeleccionada := menu(opciones)
	
	switch opcionSeleccionada{
		case 1:
			crearTablas()
			fmt.Println("\nTablas creadas\n")
			menuSQL()
		case 2:
			agregarKeys()
			fmt.Println("\nKeys insertadas en las tablas\n")
			menuSQL()
		case 3:
			eliminarKeys()
			fmt.Println("\nLas keys fueron eliminadas de las tablas\n")
			menuSQL()
		case 4:
			llenarCierres()
			fmt.Println("\nSe cargaron los cierres de tarjetas\n")
			menuSQL()
		case 5:
			llenarTablas()
			fmt.Println("\nSe llenaron las tablas con informacion\n")
			menuSQL()
		case 6:
			crearFuncionesSQL()
			fmt.Println("\nFunciones creadas\n")
			menuSQL()
		case 7:
			probarResumen()
			fmt.Println("\nLISTO! Ya puede ver las tablas actualizadas.\n")
			menuSQL()
		case 8:
			menuPrincipal()
		case -1:
			fmt.Println("\nLa opcion seleccionada es invalida\n")
			menuSQL()
	}
}

func menuNoSQL(){
	opciones := []string{
		"Cargar datos en Bolt y mostrar",
		"Volver",
	}
	fmt.Println("\nMENU NoSQL")
	fmt.Println("Por favor, seleccione una de las siguientes opciones\n")
	
	opcionSeleccionada := menu(opciones)
	
	switch opcionSeleccionada{
		case 1:
			crearDbNoSQL()
			fmt.Println("\nDatos guardados\n")
			menuNoSQL()
		case 2:
			menuPrincipal()		
		case -1:
			fmt.Println("\nLa opcion seleccionada es invalida\n")
			menuNoSQL()
	}
	
}

//Imprime las opciones en pantalla y devuelve la opcion seleccionada
//Si la opcion es invalida, devuelve -1
func menu(opciones []string) int {
	imprimirOpciones(&opciones)

	var seleccionado int
	var _, err = fmt.Scanln(&seleccionado)
	
	if err != nil {
		log.Fatal(err)
	}
	
	if seleccionado < 1 || seleccionado > len(opciones) {
		seleccionado = -1
	}

	return seleccionado
}

func imprimirOpciones(opciones *[]string) {
	for i, v := range *opciones {
		fmt.Printf("%d. %s\n", i+1, v)
	}
}


// SQL-----------------------------------------------------------------------------------------------------------------------------------------------------------------

func crearDbSQL(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
    defer sqldb.Close()

	_, err = sqldb.Query(`DROP DATABASE IF EXISTS tarjeta_de_credito`)

    if err != nil {
        log.Fatal(err)
    }

    _, err = sqldb.Exec(`CREATE DATABASE tarjeta_de_credito`)

    if err != nil {
        log.Fatal(err)
    }
}

func crearTablas() {
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	
	if err != nil {
		log.Fatal(err)
	}
	
	eliminarTablas()
	statement:=`
		CREATE TABLE cliente(
			nrocliente SERIAL,
			nombre TEXT,
			apellido TEXT,
			domicilio TEXT, 
			telefono CHAR(12)
		); 

		CREATE TABLE tarjeta ( 
			nrotarjeta CHAR(16), 
			nrocliente SERIAL, 
			validadesde CHAR(6), 
			validahasta CHAR(6),
			codseguridad CHAR(6), 
			limitecompra DECIMAL(8,2), 
			estado CHAR(10)
		);
		
		CREATE TABLE comercio ( 
			nrocomercio SERIAL, 
			nombrecomercio TEXT,
			domicilio TEXT,
			codigopostal CHAR(8),
			telefono CHAR(12)	
		); 


		CREATE TABLE compra (
			nrooperacion SERIAL,
			nrotarjeta CHAR(16),
			nrocomercio INT,
			fecha TIMESTAMP,
			monto DECIMAL(7,2),
			pagado BOOLEAN
		); 

		CREATE TABLE rechazo (
			nrorechazo SERIAL,
			nrotarjeta CHAR(16),
			nrocomercio INT,
			fecha TIMESTAMP,
			monto DECIMAL(7,2),
			motivo TEXT
		); 


		CREATE TABLE cierre (
			anio INT,
			mes INT,
			terminacion INT,
			fechainicio DATE,
			fechacierre DATE,
			fechavto DATE
		);
		 
		CREATE TABLE cabecera(
			nroresumen SERIAL,
			nombre TEXT,
			apellido TEXT,
			domicilio TEXT,
			nrotarjeta CHAR(16),
			desde DATE,
			hasta DATE,
			vence DATE,
			total decimal(8,2) 
		);


		CREATE TABLE detalle (
			nroresumen INT,
			nrolinea INT,
			fecha DATE,
			nombrecomercio TEXT,
			monto DECIMAL(7,2)
		);


		CREATE TABLE alerta (
			nroalerta SERIAL,
			nrotarjeta CHAR(16),
			fecha TIMESTAMP,
			nrorechazo SERIAL,
			codalerta INT, 
			descripcion TEXT
		);


		CREATE TABLE consumo (
			nrotarjeta CHAR(16),
			codseguridad CHAR(4),
			nrocomercio INT,
			monto DECIMAL(7,2)
		);`

	if _, err := sqldb.Query(statement); err != nil {
		log.Fatal(err)
	}
}

func eliminarTablas() {
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	
	statement:=`
		DROP TABLE IF EXISTS cliente CASCADE;
		DROP TABLE IF EXISTS tarjeta CASCADE;
		DROP TABLE IF EXISTS comercio CASCADE;
		DROP TABLE IF EXISTS compra CASCADE;
		DROP TABLE IF EXISTS rechazo CASCADE;
		DROP TABLE IF EXISTS cierre CASCADE;
		DROP TABLE IF EXISTS cabecera CASCADE;
		DROP TABLE IF EXISTS detalle CASCADE;
		DROP TABLE IF EXISTS alerta CASCADE;
		DROP TABLE IF EXISTS consumo CASCADE;`
		
	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func agregarKeys() {
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	eliminarKeys()
	statement:=`
		ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY (nrocliente);
		ALTER TABLE tarjeta ADD CONSTRAINT tarjeta_pk PRIMARY KEY (nrotarjeta);
		ALTER TABLE comercio ADD CONSTRAINT comercio_pk PRIMARY KEY (nrocomercio);
		ALTER TABLE compra ADD CONSTRAINT compra_pk PRIMARY KEY (nrooperacion); 
		ALTER TABLE rechazo ADD CONSTRAINT rechazo_pk PRIMARY KEY (nrorechazo);
		ALTER TABLE cierre ADD CONSTRAINT cierre_pk PRIMARY KEY (anio,mes,terminacion);
		ALTER TABLE cabecera ADD CONSTRAINT cabecera_pk PRIMARY KEY (nroresumen);
		ALTER TABLE detalle ADD CONSTRAINT detalle_pk PRIMARY KEY (nroresumen,nrolinea);
		ALTER TABLE alerta ADD CONSTRAINT alerta_pk PRIMARY KEY (nroalerta);

		ALTER TABLE tarjeta ADD CONSTRAINT tarjeta_nrocliente_fk FOREIGN KEY (nrocliente) REFERENCES cliente (nrocliente);
		ALTER TABLE compra ADD CONSTRAINT compra_nrotarjeta_fk FOREIGN KEY (nrotarjeta) REFERENCES tarjeta (nrotarjeta);
		ALTER TABLE compra ADD CONSTRAINT compra_nrocomercio_fk FOREIGN KEY (nrocomercio) REFERENCES comercio (nrocomercio);
		ALTER TABLE rechazo ADD CONSTRAINT rechazo_nrocomercio_fk FOREIGN KEY (nrocomercio) REFERENCES comercio (nrocomercio);
		ALTER TABLE cabecera ADD CONSTRAINT cabecera_nrotarjeta_fk FOREIGN KEY (nrotarjeta) REFERENCES tarjeta (nrotarjeta);
		ALTER TABLE alerta ADD CONSTRAINT alerta_nrorechazo_fk FOREIGN KEY (nrorechazo) REFERENCES rechazo (nrorechazo);`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func eliminarKeys() {
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_nrocliente_fk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_nrotarjeta_fk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_nrocomercio_fk;
		ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_nrocomercio_fk;
		ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS cabecera_nrotarjeta_fk;
		ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_nrorechazo_fk;

		ALTER TABLE cliente DROP CONSTRAINT IF EXISTS cliente_pk;
		ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_pk;
		ALTER TABLE comercio DROP CONSTRAINT IF EXISTS comercio_pk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_pk;
		ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_pk;
		ALTER TABLE cierre DROP CONSTRAINT IF EXISTS cierre_pk;
		ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS cabecera_pk;
		ALTER TABLE detalle DROP CONSTRAINT IF EXISTS detalle_pk;
		ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_pk;`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func llenarTablas() {
	llenarClientes()
	llenarTarjetas()
	llenarComercios()
	cargarConsumos()
}

func llenarClientes(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		INSERT INTO cliente VALUES(1, 'Juan', 'Gregorio', 'Rivadavia 2131', '11-3576-0584');
		INSERT INTO cliente VALUES(2, 'Carlos', 'Pereyra', 'José Ingenieros 445', '11-6546-1183');
		INSERT INTO cliente VALUES(3, 'José', 'Bauverde', 'Calamuchita 7245', '11-2345-6502');
		INSERT INTO cliente VALUES(4, 'Joaquín', 'Vincente', 'Córdoba 2356', '11-5687-6124');
		INSERT INTO cliente VALUES(5, 'Nicolás', 'Papini', 'Perú 234', '11-5476-1254');
		INSERT INTO cliente VALUES(6, 'Jorge', 'Tifón', 'Brasil 1451', '11-8760-4326');
		INSERT INTO cliente VALUES(7, 'Carmen Lucía', 'Estable', 'Magallanes 1557', '11-5271-6653');
		INSERT INTO cliente VALUES(8, 'Rita', 'Plano', 'Sáenz Peña 2220', '11-3576-8880');
		INSERT INTO cliente VALUES(9, 'Juan Pablo', 'Segundo', 'Vaticano 1234', '11-5421-3984');
		INSERT INTO cliente VALUES(10, 'Alex', 'Ubago', 'Roca 523', '11-3244-2365');
		INSERT INTO cliente VALUES(11, 'Giorno', 'Giovanna', 'De La Rúa 178', '11-6986-2976');
		INSERT INTO cliente VALUES(12, 'Alfio', 'Argento', 'Flores 49', '11-5976-8524');
		INSERT INTO cliente VALUES(13, 'Oriana', 'Waldorf', 'Uriburu 1145', '11-3592-1015');
		INSERT INTO cliente VALUES(14, 'Alex', 'DeLarge', 'Plus Ultra 6271', '11-2987-6521');
		INSERT INTO cliente VALUES(15, 'Mark', 'Renton', 'Escocia 3422', '11-9367-2095');
		INSERT INTO cliente VALUES(16, 'Stanley', 'Kubrick', 'Gales 411', '11-2342-9876');
		INSERT INTO cliente VALUES(17, 'Quentin', 'Tarantino', 'Gales 1232', '11-3259-8652');
		INSERT INTO cliente VALUES(18, 'Luis', 'Archibals', 'Hamington 523', '11-3298-7523');
		INSERT INTO cliente VALUES(19, 'Connor', 'McGregor', 'Las Vegas 75', '11-3532-6666');
		INSERT INTO cliente VALUES(20, 'Ricardo', 'Fort', 'Miami 240', '11-5132-4586');`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func llenarTarjetas(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		INSERT INTO tarjeta VALUES(4587265498742365, 1, '200906', '201906', 7030, 50000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4247757151747961, 2, '201208', '202208', 6452, 40000.00, 'vigente');
		INSERT INTO tarjeta VALUES(6011340854932542, 3, '201102', '202102', 2145, 60000.00, 'vigente');
		INSERT INTO tarjeta VALUES(3558097317396725, 4, '201603', '202603', 2345, 70000.00, 'suspendida');
		INSERT INTO tarjeta VALUES(3799599183628808, 5, '201702', '202702', 7862, 30000.00, 'vigente');
		INSERT INTO tarjeta VALUES(5426325300767627, 6, '202001', '203001', 5423, 60000.00, 'vigente');
		INSERT INTO tarjeta VALUES(5515632088924821, 7, '201205', '202205', 7231, 40000.00, 'anulada');
		INSERT INTO tarjeta VALUES(3712515505313708, 8, '201107', '202107', 7054, 53000.00, 'vigente');
		INSERT INTO tarjeta VALUES(3419986816351358, 9, '201405', '202405', 7860, 25000.00, 'suspendida');
		INSERT INTO tarjeta VALUES(3467854546974908, 10, '201511', '202511', 7230, 80000.00, 'vigente');
		INSERT INTO tarjeta VALUES(3471783031719268, 11, '201112', '202112', 9210, 90000.00, 'vigente');
		INSERT INTO tarjeta VALUES(3799327957181288, 12, '201104', '202104', 6421, 55000.00, 'suspendida');
		INSERT INTO tarjeta VALUES(4294962033168467, 13, '201809', '202809', 2345, 75000.00, 'anulada');
		INSERT INTO tarjeta VALUES(4451606084585520, 14, '201902', '202902', 6481, 45000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4930554755842948, 15, '202002', '203002', 2114, 60000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4400019114745860, 16, '201509', '202509', 3655, 80000.00, 'anulada');
		INSERT INTO tarjeta VALUES(4706720254967575, 17, '201712', '202712', 9554, 30000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4541524039112351, 18, '201811', '202811', 3254, 60000.00, 'suspendida');
		INSERT INTO tarjeta VALUES(4166233561788646, 19, '201610', '202610', 2542, 80000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4578965412365487, 19, '201812', '202812', 3697, 40000.00, 'vigente');
		INSERT INTO tarjeta VALUES(4067117821572520, 20, '201505', '202505', 3214, 60000.00, 'vigente');
		INSERT INTO tarjeta VALUES(5478945632540587, 20, '201702', '202702', 1021, 80000.00, 'vigente');`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func llenarComercios(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		INSERT INTO comercio VALUES(1, 'Cicilotto Hnos', 'Agustín Lara 85', 1664, '11-7593-8520');
		INSERT INTO comercio VALUES(2, 'Burger King Chacarita', 'Montaner 52', 1532, '113-5760-485');
		INSERT INTO comercio VALUES(3, 'Casa Marcelo Mueblería', 'Aurora 2665', 1772, '11-1328-9733');
		INSERT INTO comercio VALUES(4, 'La serenísima Fco. Álvarez', 'Hipólito Irigoyen 3254', 1032, '11-3437-6626');
		INSERT INTO comercio VALUES(5, 'Sueños Pañalera', 'El Cañón 32', 1253, '11-2345-2367');
		INSERT INTO comercio VALUES(6, 'Suave Vida Papelera', 'Julián Álvarez 39', 1754, '11-4026-5232');
		INSERT INTO comercio VALUES(7, 'García Librería', 'Av. del Libertador 781', 1114, '11-3251-0752');
		INSERT INTO comercio VALUES(8, 'McDonalds Tigre', 'La Rivera 2343', 1235, '11-1368-9726');
		INSERT INTO comercio VALUES(9, 'Cochería Rufino Pastor', 'Don Bosco 235', 1325, '11-3250-9265');
		INSERT INTO comercio VALUES(10, 'La Galleta Loca Panadería', 'Santos Dumont 32', 1664, '11-9098-6235');
		INSERT INTO comercio VALUES(11, 'Dos Soles Pollería', 'Demóstenes 352', 1124, '11-2350-8762');
		INSERT INTO comercio VALUES(12, 'Riquísimo Rotisería', 'Las Flores 95', 1664, '11-0970-8752');
		INSERT INTO comercio VALUES(13, 'Rincón Soñado Pizzería', 'Dardo Rocha 74', 1342, '11-2398-2873');
		INSERT INTO comercio VALUES(14, 'Cocoa Videojuegos', 'Justo Daract 3242', 1346, '11-0909-8085');
		INSERT INTO comercio VALUES(15, 'Mil820 Bar', 'Chacabuco 75', 1324, '11-42332532');
		INSERT INTO comercio VALUES(16, 'YPF Ciudadela', 'Carlos Menem 584', 1664, '11-2358-7235');
		INSERT INTO comercio VALUES(17, 'Coto Avellaneda', 'Joestar 69', 1252, '11-6297-6325');
		INSERT INTO comercio VALUES(18, 'Carrefour Maxi Espeleta', 'Estados Unidos 235', 1672, '11-4626-6244');
		INSERT INTO comercio VALUES(19, 'La Feria Persa', 'Rivadavia 2011', 1352, '11-3258-7395');
		INSERT INTO comercio VALUES(20, 'Dietética Carozo', 'Obama 845', 1362, '11-3258-7395');`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func llenarCierres(){
sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		INSERT INTO cierre VALUES(2020, 1, 0, '2020-01-02', '2020-02-02', '2020-02-05');
		INSERT INTO cierre VALUES(2020, 2, 0, '2020-02-03', '2020-03-03', '2020-03-06');
		INSERT INTO cierre VALUES(2020, 3, 0, '2020-03-04', '2020-04-04', '2020-04-07');
		INSERT INTO cierre VALUES(2020, 4, 0, '2020-04-05', '2020-05-05', '2020-05-08');
		INSERT INTO cierre VALUES(2020, 5, 0, '2020-05-06', '2020-06-06', '2020-06-09');
		INSERT INTO cierre VALUES(2020, 6, 0, '2020-06-07', '2020-07-07', '2020-07-10');
		INSERT INTO cierre VALUES(2020, 7, 0, '2020-07-08', '2020-08-08', '2020-08-11');
		INSERT INTO cierre VALUES(2020, 8, 0, '2020-08-09', '2020-09-09', '2020-09-12');
		INSERT INTO cierre VALUES(2020, 9, 0, '2020-09-10', '2020-10-10', '2020-10-13');
		INSERT INTO cierre VALUES(2020, 10, 0, '2020-10-11', '2020-11-11', '2020-11-14');
		INSERT INTO cierre VALUES(2020, 11, 0, '2020-11-12', '2020-12-12', '2020-12-15');
		INSERT INTO cierre VALUES(2020, 12, 0, '2020-12-13', '2021-01-13', '2021-01-16');

		INSERT INTO cierre VALUES(2020, 1, 1, '2020-01-03', '2020-02-03', '2020-02-06');
		INSERT INTO cierre VALUES(2020, 2, 1, '2020-02-04', '2020-03-04', '2020-03-07');
		INSERT INTO cierre VALUES(2020, 3, 1, '2020-03-05', '2020-04-05', '2020-04-08');
		INSERT INTO cierre VALUES(2020, 4, 1, '2020-04-06', '2020-05-06', '2020-05-09');
		INSERT INTO cierre VALUES(2020, 5, 1, '2020-05-07', '2020-06-07', '2020-06-10');
		INSERT INTO cierre VALUES(2020, 6, 1, '2020-06-08', '2020-07-08', '2020-07-11');
		INSERT INTO cierre VALUES(2020, 7, 1, '2020-07-09', '2020-08-09', '2020-08-12');
		INSERT INTO cierre VALUES(2020, 8, 1, '2020-08-10', '2020-09-10', '2020-09-13');
		INSERT INTO cierre VALUES(2020, 9, 1, '2020-09-11', '2020-10-11', '2020-10-14');
		INSERT INTO cierre VALUES(2020, 10, 1, '2020-10-12', '2020-11-12', '2020-11-15');
		INSERT INTO cierre VALUES(2020, 11, 1, '2020-11-13', '2020-12-13', '2020-12-16');
		INSERT INTO cierre VALUES(2020, 12, 1, '2020-12-14', '2021-01-14', '2021-01-17');

		INSERT INTO cierre VALUES(2020, 1, 2, '2020-01-04', '2020-02-04', '2020-02-07');
		INSERT INTO cierre VALUES(2020, 2, 2, '2020-02-05', '2020-03-05', '2020-03-08');
		INSERT INTO cierre VALUES(2020, 3, 2, '2020-03-06', '2020-04-06', '2020-04-09');
		INSERT INTO cierre VALUES(2020, 4, 2, '2020-04-07', '2020-05-07', '2020-05-10');
		INSERT INTO cierre VALUES(2020, 5, 2, '2020-05-08', '2020-06-08', '2020-06-11');
		INSERT INTO cierre VALUES(2020, 6, 2, '2020-06-09', '2020-07-09', '2020-07-12');
		INSERT INTO cierre VALUES(2020, 7, 2, '2020-07-10', '2020-08-10', '2020-08-13');
		INSERT INTO cierre VALUES(2020, 8, 2, '2020-08-11', '2020-09-11', '2020-09-14');
		INSERT INTO cierre VALUES(2020, 9, 2, '2020-09-12', '2020-10-12', '2020-10-15');
		INSERT INTO cierre VALUES(2020, 10, 2, '2020-10-13', '2020-11-13', '2020-11-16');
		INSERT INTO cierre VALUES(2020, 11, 2, '2020-11-14', '2020-12-14', '2020-12-17');
		INSERT INTO cierre VALUES(2020, 12, 2, '2020-12-15', '2021-01-15', '2021-01-18');

		INSERT INTO cierre VALUES(2020, 1, 3, '2020-01-05', '2020-02-05', '2020-02-08');
		INSERT INTO cierre VALUES(2020, 2, 3, '2020-02-06', '2020-03-06', '2020-03-09');
		INSERT INTO cierre VALUES(2020, 3, 3, '2020-03-07', '2020-04-07', '2020-04-10');
		INSERT INTO cierre VALUES(2020, 4, 3, '2020-04-08', '2020-05-08', '2020-05-11');
		INSERT INTO cierre VALUES(2020, 5, 3, '2020-05-09', '2020-06-09', '2020-06-12');
		INSERT INTO cierre VALUES(2020, 6, 3, '2020-06-10', '2020-07-10', '2020-07-13');
		INSERT INTO cierre VALUES(2020, 7, 3, '2020-07-11', '2020-08-11', '2020-08-14');
		INSERT INTO cierre VALUES(2020, 8, 3, '2020-08-12', '2020-09-12', '2020-09-15');
		INSERT INTO cierre VALUES(2020, 9, 3, '2020-09-13', '2020-10-13', '2020-10-16');
		INSERT INTO cierre VALUES(2020, 10, 3, '2020-10-14', '2020-11-14', '2020-11-17');
		INSERT INTO cierre VALUES(2020, 11, 3, '2020-11-15', '2020-12-15', '2020-12-18');
		INSERT INTO cierre VALUES(2020, 12, 3, '2020-12-16', '2021-01-16', '2021-01-19');

		INSERT INTO cierre VALUES(2020, 1, 4, '2020-01-06', '2020-02-06', '2020-02-09');
		INSERT INTO cierre VALUES(2020, 2, 4, '2020-02-07', '2020-03-07', '2020-03-10');
		INSERT INTO cierre VALUES(2020, 3, 4, '2020-03-08', '2020-04-08', '2020-04-11');
		INSERT INTO cierre VALUES(2020, 4, 4, '2020-04-09', '2020-05-09', '2020-05-12');
		INSERT INTO cierre VALUES(2020, 5, 4, '2020-05-10', '2020-06-10', '2020-06-13');
		INSERT INTO cierre VALUES(2020, 6, 4, '2020-06-11', '2020-07-11', '2020-07-14');
		INSERT INTO cierre VALUES(2020, 7, 4, '2020-07-12', '2020-08-12', '2020-08-15');
		INSERT INTO cierre VALUES(2020, 8, 4, '2020-08-13', '2020-09-13', '2020-09-16');
		INSERT INTO cierre VALUES(2020, 9, 4, '2020-09-14', '2020-10-14', '2020-10-17');
		INSERT INTO cierre VALUES(2020, 10, 4, '2020-10-15', '2020-11-15', '2020-11-18');
		INSERT INTO cierre VALUES(2020, 11, 4, '2020-11-16', '2020-12-16', '2020-12-19');
		INSERT INTO cierre VALUES(2020, 12, 4, '2020-12-17', '2021-01-17', '2021-01-20');

		INSERT INTO cierre VALUES(2020, 1, 5, '2020-01-07', '2020-02-07', '2020-02-10');
		INSERT INTO cierre VALUES(2020, 2, 5, '2020-02-08', '2020-03-08', '2020-03-11');
		INSERT INTO cierre VALUES(2020, 3, 5, '2020-03-09', '2020-04-09', '2020-04-12');
		INSERT INTO cierre VALUES(2020, 4, 5, '2020-04-10', '2020-05-10', '2020-05-13');
		INSERT INTO cierre VALUES(2020, 5, 5, '2020-05-11', '2020-06-11', '2020-06-14');
		INSERT INTO cierre VALUES(2020, 6, 5, '2020-06-12', '2020-07-12', '2020-07-15');
		INSERT INTO cierre VALUES(2020, 7, 5, '2020-07-13', '2020-08-13', '2020-08-16');
		INSERT INTO cierre VALUES(2020, 8, 5, '2020-08-14', '2020-09-14', '2020-09-17');
		INSERT INTO cierre VALUES(2020, 9, 5, '2020-09-15', '2020-10-15', '2020-10-18');
		INSERT INTO cierre VALUES(2020, 10, 5, '2020-10-16', '2020-11-16', '2020-11-19');
		INSERT INTO cierre VALUES(2020, 11, 5, '2020-11-17', '2020-12-17', '2020-12-20');
		INSERT INTO cierre VALUES(2020, 12, 5, '2020-12-18', '2021-01-18', '2021-01-21');

		INSERT INTO cierre VALUES(2020, 1, 6, '2020-01-08', '2020-02-08', '2020-02-11');
		INSERT INTO cierre VALUES(2020, 2, 6, '2020-02-09', '2020-03-09', '2020-03-12');
		INSERT INTO cierre VALUES(2020, 3, 6, '2020-03-10', '2020-04-10', '2020-04-13');
		INSERT INTO cierre VALUES(2020, 4, 6, '2020-04-11', '2020-05-11', '2020-05-14');
		INSERT INTO cierre VALUES(2020, 5, 6, '2020-05-12', '2020-06-12', '2020-06-15');
		INSERT INTO cierre VALUES(2020, 6, 6, '2020-06-13', '2020-07-13', '2020-07-16');
		INSERT INTO cierre VALUES(2020, 7, 6, '2020-07-14', '2020-08-14', '2020-08-17');
		INSERT INTO cierre VALUES(2020, 8, 6, '2020-08-15', '2020-09-15', '2020-09-18');
		INSERT INTO cierre VALUES(2020, 9, 6, '2020-09-16', '2020-10-16', '2020-10-19');
		INSERT INTO cierre VALUES(2020, 10, 6, '2020-10-17', '2020-11-17', '2020-11-20');
		INSERT INTO cierre VALUES(2020, 11, 6, '2020-11-18', '2020-12-18', '2020-12-21');
		INSERT INTO cierre VALUES(2020, 12, 6, '2020-12-19', '2021-01-19', '2021-01-22');

		INSERT INTO cierre VALUES(2020, 1, 7, '2020-01-09', '2020-02-09', '2020-02-12');
		INSERT INTO cierre VALUES(2020, 2, 7, '2020-02-10', '2020-03-10', '2020-03-13');
		INSERT INTO cierre VALUES(2020, 3, 7, '2020-03-11', '2020-04-11', '2020-04-14');
		INSERT INTO cierre VALUES(2020, 4, 7, '2020-04-12', '2020-05-12', '2020-05-15');
		INSERT INTO cierre VALUES(2020, 5, 7, '2020-05-13', '2020-06-13', '2020-06-16');
		INSERT INTO cierre VALUES(2020, 6, 7, '2020-06-14', '2020-07-14', '2020-07-17');
		INSERT INTO cierre VALUES(2020, 7, 7, '2020-07-15', '2020-08-15', '2020-08-18');
		INSERT INTO cierre VALUES(2020, 8, 7, '2020-08-16', '2020-09-16', '2020-09-19');
		INSERT INTO cierre VALUES(2020, 9, 7, '2020-09-17', '2020-10-17', '2020-10-20');
		INSERT INTO cierre VALUES(2020, 10, 7, '2020-10-18', '2020-11-18', '2020-11-21');
		INSERT INTO cierre VALUES(2020, 11, 7, '2020-11-19', '2020-12-19', '2020-12-22');
		INSERT INTO cierre VALUES(2020, 12, 7, '2020-12-20', '2021-01-20', '2021-01-23');

		INSERT INTO cierre VALUES(2020, 1, 8, '2020-01-10', '2020-02-10', '2020-02-13');
		INSERT INTO cierre VALUES(2020, 2, 8, '2020-02-11', '2020-03-11', '2020-03-14');
		INSERT INTO cierre VALUES(2020, 3, 8, '2020-03-12', '2020-04-12', '2020-04-15');
		INSERT INTO cierre VALUES(2020, 4, 8, '2020-04-13', '2020-05-13', '2020-05-16');
		INSERT INTO cierre VALUES(2020, 5, 8, '2020-05-14', '2020-06-14', '2020-06-17');
		INSERT INTO cierre VALUES(2020, 6, 8, '2020-06-15', '2020-07-15', '2020-07-18');
		INSERT INTO cierre VALUES(2020, 7, 8, '2020-07-16', '2020-08-16', '2020-08-19');
		INSERT INTO cierre VALUES(2020, 8, 8, '2020-08-17', '2020-09-17', '2020-09-20');
		INSERT INTO cierre VALUES(2020, 9, 8, '2020-09-18', '2020-10-18', '2020-10-21');
		INSERT INTO cierre VALUES(2020, 10, 8, '2020-10-19', '2020-11-19', '2020-11-22');
		INSERT INTO cierre VALUES(2020, 11, 8, '2020-11-20', '2020-12-20', '2020-12-23');
		INSERT INTO cierre VALUES(2020, 12, 8, '2020-12-21', '2021-01-21', '2021-01-24');

		INSERT INTO cierre VALUES(2020, 1, 9, '2020-01-11', '2020-02-11', '2020-02-14');
		INSERT INTO cierre VALUES(2020, 2, 9, '2020-02-12', '2020-03-12', '2020-03-15');
		INSERT INTO cierre VALUES(2020, 3, 9, '2020-03-13', '2020-04-13', '2020-04-16');
		INSERT INTO cierre VALUES(2020, 4, 9, '2020-04-14', '2020-05-14', '2020-05-17');
		INSERT INTO cierre VALUES(2020, 5, 9, '2020-05-15', '2020-06-15', '2020-06-18');
		INSERT INTO cierre VALUES(2020, 6, 9, '2020-06-16', '2020-07-16', '2020-07-19');
		INSERT INTO cierre VALUES(2020, 7, 9, '2020-07-17', '2020-08-17', '2020-08-20');
		INSERT INTO cierre VALUES(2020, 8, 9, '2020-08-18', '2020-09-18', '2020-09-21');
		INSERT INTO cierre VALUES(2020, 9, 9, '2020-09-19', '2020-10-19', '2020-10-22');
		INSERT INTO cierre VALUES(2020, 10, 9, '2020-10-20', '2020-11-20', '2020-11-23');
		INSERT INTO cierre VALUES(2020, 11, 9, '2020-11-21', '2020-12-21', '2020-12-24');
		INSERT INTO cierre VALUES(2020, 12, 9, '2020-12-22', '2021-01-22', '2021-01-25');`
		
	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func cargarConsumos(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		INSERT INTO consumo VALUES(4587265498742365, 7030, 1, 530.00); --compra autorizada
		INSERT INTO consumo VALUES(4247757151747961, 6452, 11, 41000.00); --se pasa del monto que tiene para usar
		INSERT INTO consumo VALUES(3558097173006725, 2342, 4, 100.00); --no existe tarjeta 
		INSERT INTO consumo VALUES(4587265498742365, 7031, 7, 590.00); -- codigo de seguridad invalido
		INSERT INTO consumo VALUES(4541524039112351, 3254, 5, 10000.00); -- tarjeta suspendida
		INSERT INTO consumo VALUES(5478945632540587, 1021, 6, 15000.00); -- tarjeta expirada
		INSERT INTO consumo VALUES(4587265498742365, 7030, 9, 5000.00); -- compra autorizada en otro código postal
		INSERT INTO consumo VALUES(4587265498742365, 7030, 12, 500.00); -- compra autorizada en el mismo código postal`
		
	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func crearFuncionesSQL(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	statement:=`
		CREATE OR REPLACE FUNCTION autorizacion_de_compra(nro_tarjeta CHAR(16), cod_seguridad CHAR(6), nro_comercio INT, monto DECIMAL(7,2)) RETURNS BOOLEAN AS $$ 
		DECLARE
			v RECORD; 
			total INT;
			aux1 RECORD;
			aux2 RECORD;
			aux3 RECORD;
			aux4 RECORD;
			aux5 RECORD;
			fecha TEXT;
		BEGIN

			-- Tarjeta inválida --

			SELECT * 
			INTO aux1
			FROM tarjeta t 
			WHERE t.nrotarjeta = nro_tarjeta AND 
				t.estado::TEXT != 'anulada'::TEXT;
			IF NOT FOUND THEN
				INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Tarjeta no válida o vigente');
				RETURN FALSE; 
			ELSE

			-- Código de seguridad inválido --

			SELECT * 
			INTO aux2
			FROM tarjeta t 
			WHERE t.codseguridad = cod_seguridad AND
				t.nrotarjeta = nro_tarjeta;
			IF NOT FOUND THEN
				INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Código de seguridad inválido');
				RETURN FALSE; 
			ELSE

			-- Límite de tarjeta excedido --
			
			total := 0;

			FOR v IN SELECT * 
				FROM compra c, tarjeta t 
				WHERE t.nrotarjeta = nro_tarjeta AND 
					c.nrotarjeta = t.nrotarjeta AND 
					c.pagado = false;
			LOOP
				total := total + v.monto;
			END LOOP;

			total := total + monto;

			SELECT *
			INTO aux3
			FROM tarjeta t 
			WHERE total > t.limitecompra AND 
				t.nrotarjeta = nro_tarjeta;
			IF FOUND THEN
				INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Supera límite de tarjeta');
				RETURN FALSE; 
			ELSE

			-- Tarjeta vencida --
			
				fecha := TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMM');

			SELECT *
			INTO aux4
			FROM tarjeta t 
			WHERE t.nrotarjeta = nro_tarjeta AND 
					fecha >= t.validahasta; 
			IF FOUND THEN
				INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Plazo de vigencia expirado');
				RETURN FALSE;
			ELSE

			-- Tarjeta suspendida --

			SELECT *
			INTO aux5
			FROM tarjeta t 
			WHERE t.nrotarjeta = nro_tarjeta AND 
				t.estado::TEXT = 'suspendida'::TEXT;
			IF FOUND THEN
				INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'La tarjeta se encuentra suspendida');
				RETURN FALSE;
			
			END IF;
			END IF;
			END IF;
			END IF;
			END IF;

			-- Compra autorizada --
			
			INSERT INTO compra VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, false);

			RETURN TRUE;

		END;
		$$ LANGUAGE PLPGSQL;

		-- Recibe como parámetros el número de cliente y el periodo del año, y guarda
		-- en las tablas que corresponde los datos del resumen con el nombre y apellido,
		-- dirección, número de tarjeta, periodo del resumen, fecha de vencimiento, todas
		-- las compras del periodo, y total a pagar
		CREATE OR REPLACE FUNCTION resumen_cliente(nro_cliente INT, mes TEXT, anio TEXT) RETURNS VOID AS $$
		DECLARE 
			nombre TEXT;
			apellido TEXT;
			domicilio TEXT;
			aux RECORD;
			aux2 RECORD;
			v RECORD;
		BEGIN 
			-- Agarra los datos del cliente
			SELECT * INTO aux FROM cliente c WHERE c.nrocliente = nro_cliente;
				nombre := aux.nombre;
				apellido := aux.apellido;
				domicilio := aux.domicilio;
			
			-- Llama a la función auxiliar con los datos del cliente y la tarjeta en cuestión
			FOR v IN SELECT * 
				FROM tarjeta 
				WHERE nrocliente = nro_cliente 
			LOOP
				SELECT resumen_tarjeta_cliente(v.nrocliente, v.nrotarjeta, nombre, apellido, domicilio, mes, anio) INTO aux2;
			END LOOP;

		END; 
		$$ LANGUAGE PLPGSQL; 

		-- Función auxiliar de resumen_cliente. Genera el resumen correspondiente para cada tarjeta del cliente
		CREATE OR REPLACE FUNCTION resumen_tarjeta_cliente(nro_cliente INT, nro_tarjeta CHAR(16), nombre TEXT, apellido TEXT, domicilio TEXT, mes TEXT, anio TEXT) RETURNS VOID AS $$
		DECLARE
			a_pagar INT; 
			aux RECORD;
			aux2 INT;
			nro_linea INT;
			desde DATE;
			hasta DATE;
			vence DATE; 
			v RECORD;
			terminacion_tarjeta INT;
			nombre_comercio TEXT;
			nro_resumen INT;
		BEGIN
			-- Calcula si existen compras con la tarjeta --
			
			SELECT COUNT(*) FROM compra WHERE nrotarjeta = nro_tarjeta INTO aux2;
			IF aux2 = 0 THEN
			RETURN;
			ELSE

			-- Calcula el total a pagar --
			
			a_pagar := 0;

			FOR v IN SELECT * 
				FROM tarjeta t, compra c 
				WHERE t.nrotarjeta = c.nrotarjeta AND 
					t.nrocliente = nro_cliente AND 
					anio = TO_CHAR(c.fecha, 'YYYY') AND
					mes = TO_CHAR(c.fecha, 'MM')
			LOOP

				a_pagar = a_pagar + v.monto;

			END LOOP; 

			-- Cabecera (una por tarjeta) --

			terminacion_tarjeta := RIGHT(nro_tarjeta, 1);

			SELECT * 
			INTO aux 
			FROM cierre c 
			WHERE c.terminacion = terminacion_tarjeta;
			IF FOUND THEN
				desde := aux.fechainicio;
				hasta := aux.fechacierre;
				vence := aux.fechavto;
			END IF;

			INSERT INTO cabecera 
				VALUES(DEFAULT, nombre, apellido, domicilio, nro_tarjeta, desde, hasta, vence, a_pagar);
			
			-- Detalle (una por cada compra realizada) --
			
			nro_linea := 1;
			SELECT MAX(nroresumen) INTO nro_resumen FROM cabecera;

			FOR v IN SELECT * 
				FROM tarjeta t, compra c, comercio m 
				WHERE t.nrotarjeta = c.nrotarjeta AND 
					t.nrocliente = nro_cliente AND 
					c.nrocomercio = m.nrocomercio AND
					anio = TO_CHAR(c.fecha, 'YYYY') AND
					mes = TO_CHAR(c.fecha, 'MM')
			LOOP
				
				UPDATE compra SET pagado = TRUE WHERE nrooperacion = v.nrooperacion; 

				INSERT INTO detalle VALUES(nro_resumen, nro_linea, v.fecha, v.nombrecomercio, v.monto);
				nro_linea := nro_linea + 1;

			END LOOP;
			END IF;

		END;
		$$ LANGUAGE PLPGSQL; 

		-- Alertas --

		-- Se produce una alerta cuando se genera un rechazo
		CREATE OR REPLACE FUNCTION alerta_0() RETURNS TRIGGER AS $$
		DECLARE
		BEGIN
			INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 0, 'Se ha producido un rechazo');

			RETURN NULL;
		END;
		$$ LANGUAGE PLPGSQL;

		CREATE TRIGGER alerta_0_trigger AFTER INSERT ON rechazo
		FOR EACH ROW EXECUTE PROCEDURE alerta_0();

		-- Se produce una alerta cuando se registran dos compras con la misma tarjeta en un lapso menor de 1 minuto en comercios con el mismo código postal
		CREATE OR REPLACE FUNCTION alerta_1() RETURNS TRIGGER AS $$
		DECLARE
			ultima_compra INT;
			diferencia INT;
			nroalerta INT;
			ult_cod_postal CHAR(8);
			new_cod_postal CHAR(8);
		BEGIN
			PERFORM
				FROM
				compra c                                                        
				INNER JOIN comercio m 
			ON m.nrocomercio = c.nrocomercio
				WHERE c.nrotarjeta = new.nrotarjeta AND
				c.nrocomercio != new.nrocomercio AND
			m.codigopostal = (SELECT codigopostal
						FROM comercio
						WHERE nrocomercio = new.nrocomercio) AND
					EXTRACT(EPOCH FROM(new.fecha - c.fecha)) < 60;
				IF FOUND THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, -1, 1, 'Se registraron dos compras con la misma tarjeta en un lapso menor a un minuto en comercios ubicados en el mismo código postal');
			END IF;

			RETURN NULL;
		END;
		$$ LANGUAGE PLPGSQL;

		CREATE TRIGGER alerta_1_trigger BEFORE INSERT ON compra 
		FOR EACH ROW EXECUTE PROCEDURE alerta_1();

		-- Se produce una alerta cuando se registran dos compras con la misma tarjeta en un lapso menor de 5 minutos en comercios con distinto código postal
		CREATE OR REPLACE FUNCTION alerta_5() RETURNS TRIGGER AS $$
		DECLARE
			ultima_compra INT;
			diferencia INT;
			nroalerta INT;
			old_cod_postal CHAR(8);
			new_cod_postal CHAR(8);
		BEGIN
			PERFORM
				FROM compra c
				INNER JOIN comercio m 
			ON m.nrocomercio = c.nrocomercio
				WHERE c.nrotarjeta = new.nrotarjeta AND 
			m.codigopostal != (SELECT codigopostal
					FROM comercio
						WHERE nrocomercio = new.nrocomercio) AND 
				EXTRACT(EPOCH FROM(new.fecha - c.fecha)) < 300;
			IF FOUND THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, -1, 5, 'Se registraron dos compras con la misma tarjeta en un lapso menor a cinco minutos en comercios ubicados en distinto código postal');
			END IF;

			RETURN NULL;
		END;
		$$ LANGUAGE PLPGSQL;

		CREATE TRIGGER alerta_5_trigger BEFORE INSERT ON compra 
		FOR EACH ROW EXECUTE PROCEDURE alerta_5();

		-- Se produce una alerta si una tarjeta registra dos rechazos por exceso de límite en el mismo día, y la tarjeta se suspende
		CREATE OR REPLACE FUNCTION alerta_32() RETURNS TRIGGER AS $$
		DECLARE
			ultimo_rechazo INT;
			aux RECORD;
			diferencia INT;
		BEGIN
			IF new.motivo::TEXT = 'Supera límite de tarjeta'::TEXT THEN

				SELECT MAX(nrorechazo) INTO ultimo_rechazo FROM rechazo;

				SELECT * 
				INTO aux 
				FROM rechazo 
				WHERE nrorechazo = ultimo_rechazo AND 
					motivo = new.motivo;

				diferencia := EXTRACT(EPOCH FROM(new.fecha - aux.fecha));

				IF diferencia < 86400 THEN --Argh

					UPDATE tarjeta SET estado = 'suspendida' WHERE nrotarjeta = new.nrotarjeta;

					INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Se registraron dos rechazos por exceso de límite en el mismo día. La tarjeta fue suspendida preventivamente.');
				END IF;
			
			END IF;

			RETURN NULL;
		END;
		$$ LANGUAGE PLPGSQL;

		CREATE TRIGGER alerta_32_trigger AFTER INSERT ON rechazo 
		FOR EACH ROW EXECUTE PROCEDURE alerta_32();
		
		
		CREATE OR REPLACE FUNCTION probar_consumos() RETURNS VOID AS $$
		DECLARE
			v RECORD;
			aux RECORD;
		BEGIN

			FOR v IN (SELECT * FROM consumo)
			LOOP

				SELECT autorizacion_de_compra(v.nrotarjeta, v.codseguridad, v.nrocomercio, v.monto) INTO aux;

			END LOOP;

		END;
		$$ LANGUAGE PLPGSQL;
		
		
		CREATE OR REPLACE FUNCTION probar_resumen() RETURNS VOID AS $$ 
		DECLARE
			nro_cliente INT;
			v RECORD;
			aux RECORD;
			mes TEXT;
			anio TEXT;
		BEGIN
			
			FOR v IN SELECT * FROM cliente WHERE nrocliente = 1 
			LOOP
				nro_cliente := v.nrocliente;
				mes := TO_CHAR(CURRENT_TIMESTAMP, 'MM');
				anio := TO_CHAR(CURRENT_TIMESTAMP, 'YYYY');

				SELECT resumen_cliente(nro_cliente, mes, anio) INTO aux;

			   END LOOP;

		END;
		$$ LANGUAGE PLPGSQL;`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}

}

func probarConsumos(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	
	statement:=`SELECT probar_consumos();`
		
	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func probarResumen(){
	sqldb,err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjeta_de_credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	
	probarConsumos()
	statement:=`SELECT probar_resumen();`
		
		
	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
	
}

//NoSQL----------------------------------------------------------------------------------------------------------------------------------------------------------------

func crearDbNoSQL() {
	boltdb, err := bolt.Open("tarjetas.db", 0600, nil)
    if err != nil {
        log.Fatal(err)
    }
    defer boltdb.Close()
    
    cliente1 := Cliente{1, "Naruto", "Uzumaki", "Konohagakure 222", "11-6543-1243"}
    cliente2 := Cliente{2, "Sasuke", "Uchiha", "Konohagakure 842", "11-6345-6231"}
    cliente3 := Cliente{2, "Sakura", "Haruno", "Konohagakure 492", "11-9763-8420"}
    
    dataCliente1, err := json.Marshal(cliente1)
    if err != nil {
		log.Fatal(err)
	}
	
	dataCliente2, err := json.Marshal(cliente2)
    if err != nil {
		log.Fatal(err)
	}
	
	dataCliente3, err := json.Marshal(cliente3)
    if err != nil {
		log.Fatal(err)
	}
	
	CreateUpdate(boltdb, "clientes", []byte(strconv.Itoa(cliente1.NroCliente)), dataCliente1)
	CreateUpdate(boltdb, "clientes", []byte(strconv.Itoa(cliente2.NroCliente)), dataCliente2)
	CreateUpdate(boltdb, "clientes", []byte(strconv.Itoa(cliente3.NroCliente)), dataCliente3)
	
	readCliente1, _ := ReadUnique(boltdb, "clientes", []byte(strconv.Itoa(cliente1.NroCliente)))
	readCliente2, _ := ReadUnique(boltdb, "clientes", []byte(strconv.Itoa(cliente2.NroCliente)))
	readCliente3, _ := ReadUnique(boltdb, "clientes", []byte(strconv.Itoa(cliente3.NroCliente)))
	
	fmt.Println("\n Clientes:")
	fmt.Printf("%s\n",readCliente1)
	fmt.Printf("%s\n",readCliente2)
	fmt.Printf("%s\n",readCliente3)
	
	
	tarjeta1 := Tarjeta{"4587265498742365", 1, "200906", "201906", "7030", 50000.00, "vigente"}
    tarjeta2 := Tarjeta{"4247757151747961", 2, "201208", "202208", "6452", 40000.00, "vigente"}
    tarjeta3 := Tarjeta{"6011340854932542", 3, "201102", "202102", "2145", 60000.00, "vigente"}
    
    dataTarjeta1, err := json.Marshal(tarjeta1)
    if err != nil {
		log.Fatal(err)
	}
	
	dataTarjeta2, err := json.Marshal(tarjeta2)
    if err != nil {
		log.Fatal(err)
	}
	
	dataTarjeta3, err := json.Marshal(tarjeta3)
    if err != nil {
		log.Fatal(err)
	}
	
	CreateUpdate(boltdb, "tarjetas", []byte(tarjeta1.NroTarjeta), dataTarjeta1)
	CreateUpdate(boltdb, "tarjetas", []byte(tarjeta2.NroTarjeta), dataTarjeta2)
	CreateUpdate(boltdb, "tarjetas", []byte(tarjeta3.NroTarjeta), dataTarjeta3)
	
	readTarjeta1, _ := ReadUnique(boltdb, "tarjetas", []byte(tarjeta1.NroTarjeta))
	readTarjeta2, _ := ReadUnique(boltdb, "tarjetas", []byte(tarjeta2.NroTarjeta))
	readTarjeta3, _ := ReadUnique(boltdb, "tarjetas", []byte(tarjeta3.NroTarjeta))
	
	fmt.Println("\n Tarjetas:")
	fmt.Printf("%s\n",readTarjeta1)
	fmt.Printf("%s\n",readTarjeta2)
	fmt.Printf("%s\n",readTarjeta3)
	
	
	comercio1 := Comercio{1, "Cicilotto Hnos", "Agustin Lara 87", "1664", "1175938520"}
    comercio2 := Comercio{2, "Burger King Chacarita", "Montaner 52", "1532", "1113289733"}
    comercio3 := Comercio{3, "Casa Marcelo Muebleria", "Aurora 2665", "1772", "1113289733"}
    
    dataComercio1, err := json.Marshal(comercio1)
    if err != nil {
		log.Fatal(err)
	}
	
	dataComercio2, err := json.Marshal(comercio2)
    if err != nil {
		log.Fatal(err)
	}
	
	dataComercio3, err := json.Marshal(comercio3)
    if err != nil {
		log.Fatal(err)
	}
	
	CreateUpdate(boltdb, "comercios", []byte(strconv.Itoa(comercio1.NroComercio)), dataComercio1)
	CreateUpdate(boltdb, "comercios", []byte(strconv.Itoa(comercio2.NroComercio)), dataComercio2)
	CreateUpdate(boltdb, "comercios", []byte(strconv.Itoa(comercio3.NroComercio)), dataComercio3)
	
	readComercio1, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(comercio1.NroComercio)))
	readComercio2, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(comercio2.NroComercio)))
	readComercio3, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(comercio3.NroComercio)))
	
	fmt.Println("\n Comercios:")
	fmt.Printf("%s\n",readComercio1)
	fmt.Printf("%s\n",readComercio2)
	fmt.Printf("%s\n",readComercio3)
	
	
	compra1 := Compra{1, "4587265498742365", 1, "2020-05-22", 112.13, true}
    compra2 := Compra{3, "4247757151747961", 2, "2020-04-01", 1000.50, true}
    compra3 := Compra{5, "4587265498742365", 3, "2020-12-01", 1250.40, true}
    
    dataCompra1, err := json.Marshal(compra1)
    if err != nil {
		log.Fatal(err)
	}
	
	dataCompra2, err := json.Marshal(compra2)
    if err != nil {
		log.Fatal(err)
	}
	
	dataCompra3, err := json.Marshal(compra3)
    if err != nil {
		log.Fatal(err)
	}
	
	CreateUpdate(boltdb, "compras", []byte(strconv.Itoa(compra1.NroOperacion)), dataCompra1)
	CreateUpdate(boltdb, "compras", []byte(strconv.Itoa(compra2.NroOperacion)), dataCompra2)
	CreateUpdate(boltdb, "compras", []byte(strconv.Itoa(compra3.NroOperacion)), dataCompra3)
	
	readCompra1, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(compra1.NroOperacion)))
	readCompra2, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(compra2.NroOperacion)))
	readCompra3, _ := ReadUnique(boltdb, "comercios", []byte(strconv.Itoa(compra3.NroOperacion)))
	
	fmt.Println("\n Compras:")
	fmt.Printf("%s\n",readCompra1)
	fmt.Printf("%s\n",readCompra2)
	fmt.Printf("%s\n",readCompra3)

}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
    // abre transacción de escritura
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

    err = b.Put(key, val)
    if err != nil {
        return err
    }

    // cierra transacción
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
    var buf []byte

    // abre una transacción de lectura
    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(bucketName))
        buf = b.Get(key)
        return nil
    })

    return buf, err
}


