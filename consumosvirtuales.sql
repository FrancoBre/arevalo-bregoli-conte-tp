\c tarjetadecredito

CREATE TABLE consumo_virtual (
	nrotarjeta CHAR(16),
	codseguridad CHAR(6),
	nrocomercio INT,
	monto DECIMAL(7,2)
);

INSERT INTO consumo_virtual VALUES(4587265498742365, 7030, 11, 500.00);

INSERT INTO consumo_virtual VALUES(3467854546974908, 7230, 11, 3000.00);

-- Código de seguridad incorrecto
INSERT INTO consumo_virtual VALUES(4400019114745860, 3656, 11, 500.00);
