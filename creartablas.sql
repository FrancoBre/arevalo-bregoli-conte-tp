DROP DATABASE IF EXISTS tarjetadecredito;

CREATE DATABASE tarjetadecredito;

\c tarjetadecredito;

CREATE TABLE cliente(
	nrocliente SERIAL,
	nombre TEXT,
	apellido TEXT,
	domicilio TEXT, 
	telefono CHAR(12)
); 

-- ALTER TABLE cliente
-- ALTER COLUMN nrocliente TYPE SERIAL; 


CREATE TABLE tarjeta ( 
	nrotarjeta CHAR(16), 
	nrocliente SERIAL, 
	validadesde CHAR(6), 
	validahasta CHAR(6),
	codseguridad CHAR(6), 
	limitecompra DECIMAL(8,2), 
	estado CHAR(10)
);

-- ALTER TABLE tarjeta
--    ALTER COLUMN nrocliente TYPE SERIAL;

CREATE TABLE comercio ( 
	nrocomercio SERIAL, 
	nombrecomercio TEXT,
	domicilio TEXT,
	codigopostal CHAR(8),
	telefono CHAR(12)	
); 

-- ALTER TABLE comercio
--    ALTER COLUMN nrocomercio TYPE SERIAL;

CREATE TABLE compra (
	nrooperacion SERIAL,
	nrotarjeta CHAR(16),
	nrocomercio INT,
	fecha TIMESTAMP,
	monto DECIMAL(7,2),
	pagado BOOLEAN
); 

-- ALTER TABLE compra
--    ALTER COLUMN nrooperacion TYPE SERIAL;

-- ALTER TABLE compra
--    ALTER COLUMN nrocomercio TYPE SERIAL;

CREATE TABLE rechazo (
	nrorechazo SERIAL,
	nrotarjeta CHAR(16),
	nrocomercio INT,
	fecha TIMESTAMP,
	monto DECIMAL(7,2),
	motivo TEXT
); 

-- ALTER TABLE rechazo
--    ALTER COLUMN nrorechazo TYPE SERIAL;

-- ALTER TABLE rechazo
--    ALTER COLUMN nrocomercio TYPE SERIAL;

CREATE TABLE cierre (
	anio INT,
	mes INT,
	terminacion INT,
	fechainicio DATE,
	fechacierre DATE,
	fechavto DATE
);
 
CREATE TABLE cabecera(
	nroresumen SERIAL,
	nombre TEXT,
	apellido TEXT,
	domicilio TEXT,
	nrotarjeta CHAR(16),
	desde DATE,
	hasta DATE,
	vence DATE,
	total decimal(8,2) 
);

-- ALTER TABLE cabecera
--    ALTER COLUMN nroresumen TYPE SERIAL;

CREATE TABLE detalle (
	nroresumen INT,
	nrolinea INT,
	fecha DATE,
	nombrecomercio TEXT,
	monto DECIMAL(7,2)
);

-- ALTER TABLE detalle
--    ALTER COLUMN nroresumen TYPE SERIAL;

CREATE TABLE alerta (
	nroalerta SERIAL,
	nrotarjeta CHAR(16),
	fecha TIMESTAMP,
	nrorechazo SERIAL,
	codalerta INT, 
	descripcion TEXT
);

-- ALTER TABLE alerta
--    ALTER COLUMN nroalerta TYPE SERIAL;

-- ALTER TABLE alerta
--    ALTER COLUMN nrorechazo TYPE SERIAL;

CREATE TABLE consumo (
	nrotarjeta CHAR(16),
	codseguridad CHAR(4),
	nrocomercio INT,
	monto DECIMAL(7,2)
);

-- ALTER TABLE consumo
--    ALTER COLUMN nrocomercio TYPE SERIAL;
