\c tarjetadecredito;

INSERT INTO consumo VALUES(4587265498742365, 7030, 1, 530.00); --compra autorizada
INSERT INTO consumo VALUES(4247757151747961, 6452, 11, 41000.00); --se pasa del monto que tiene para usar
INSERT INTO consumo VALUES(3558097173006725, 2342, 4, 100.00); --no existe tarjeta 
INSERT INTO consumo VALUES(4587265498742365, 7031, 7, 590.00); -- codigo de seguridad invalido
INSERT INTO consumo VALUES(4541524039112351, 3254, 5, 10000.00); -- tarjeta suspendida
INSERT INTO consumo VALUES(5478945632540587, 1021, 6, 15000.00); -- tarjeta expirada
INSERT INTO consumo VALUES(4587265498742365, 7030, 9, 5000.00); -- compra autorizada en otro código postal
INSERT INTO consumo VALUES(4587265498742365, 7030, 12, 500.00); -- compra autorizada en el mismo código postal
