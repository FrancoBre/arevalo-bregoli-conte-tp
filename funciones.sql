\c tarjetadecredito

-- Funciones --

-- Recibe los datos de una compra y devuelve true si se autoriza la compra y
-- false si se la rechaza
CREATE OR REPLACE FUNCTION autorizacion_de_compra(nro_tarjeta CHAR(16), cod_seguridad CHAR(6), nro_comercio INT, monto DECIMAL(7,2)) RETURNS BOOLEAN AS $$ 
DECLARE
	v RECORD; 
	total INT;
	aux1 RECORD;
	aux2 RECORD;
	aux3 RECORD;
	aux4 RECORD;
	aux5 RECORD;
	fecha TEXT;
BEGIN

	-- Tarjeta inválida --

	SELECT * 
	INTO aux1
	FROM tarjeta t 
	WHERE t.nrotarjeta = nro_tarjeta AND 
		t.estado::TEXT != 'anulada'::TEXT;
	IF NOT FOUND THEN
		INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Tarjeta no válida o vigente');
		RETURN FALSE; 
	ELSE

	-- Código de seguridad inválido --

	SELECT * 
	INTO aux2
	FROM tarjeta t 
	WHERE t.codseguridad = cod_seguridad AND
		t.nrotarjeta = nro_tarjeta;
	IF NOT FOUND THEN
		INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Código de seguridad inválido');
		RETURN FALSE; 
	ELSE

	-- Límite de tarjeta excedido --
	
	total := 0;

	FOR v IN SELECT * 
		FROM compra c, tarjeta t 
		WHERE t.nrotarjeta = nro_tarjeta AND 
			c.nrotarjeta = t.nrotarjeta AND 
			c.pagado = true 
	LOOP
		total := total + v.monto;
	END LOOP;

	total := total + monto;

	SELECT *
	INTO aux3
	FROM tarjeta t 
	WHERE total > t.limitecompra AND 
		t.nrotarjeta = nro_tarjeta;
	IF FOUND THEN
		INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Supera límite de tarjeta');
		RETURN FALSE; 
	ELSE

	-- Tarjeta vencida --
	
        fecha := TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMM');

	SELECT *
	INTO aux4
	FROM tarjeta t 
	WHERE t.nrotarjeta = nro_tarjeta AND 
			fecha >= t.validahasta; 
	IF FOUND THEN
		INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'Plazo de vigencia expirado');
		RETURN FALSE;
	ELSE

	-- Tarjeta suspendida --

	SELECT *
	INTO aux5
	FROM tarjeta t 
	WHERE t.nrotarjeta = nro_tarjeta AND 
		t.estado::TEXT = 'suspendida'::TEXT;
	IF FOUND THEN
		INSERT INTO rechazo VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, 'La tarjeta se encuentra suspendida');
		RETURN FALSE;
	
	END IF;
	END IF;
	END IF;
	END IF;
	END IF;

	-- Compra autorizada --
	
	INSERT INTO compra VALUES(DEFAULT, nro_tarjeta, nro_comercio, CURRENT_TIMESTAMP, monto, false);

	RETURN TRUE;

END;
$$ LANGUAGE PLPGSQL;

-- Recibe como parámetros el número de cliente y el periodo del año, y guarda
-- en las tablas que corresponde los datos del resumen con el nombre y apellido,
-- dirección, número de tarjeta, periodo del resumen, fecha de vencimiento, todas
-- las compras del periodo, y total a pagar
CREATE OR REPLACE FUNCTION resumen_cliente(nro_cliente INT, mes TEXT, anio TEXT) RETURNS VOID AS $$
DECLARE 
	nombre TEXT;
	apellido TEXT;
	domicilio TEXT;
	aux RECORD;
	aux2 RECORD;
	v RECORD;
BEGIN 
	-- Agarra los datos del cliente
	SELECT * INTO aux FROM cliente c WHERE c.nrocliente = nro_cliente;
		nombre := aux.nombre;
		apellido := aux.apellido;
		domicilio := aux.domicilio;
	
	-- Llama a la función auxiliar con los datos del cliente y la tarjeta en cuestión
	FOR v IN SELECT * 
		FROM tarjeta 
		WHERE nrocliente = nro_cliente 
	LOOP
		SELECT resumen_tarjeta_cliente(v.nrocliente, v.nrotarjeta, nombre, apellido, domicilio, mes, anio) INTO aux2;
	END LOOP;

END; 
$$ LANGUAGE PLPGSQL; 

-- Función auxiliar de resumen_cliente. Genera el resumen correspondiente para cada tarjeta del cliente
CREATE OR REPLACE FUNCTION resumen_tarjeta_cliente(nro_cliente INT, nro_tarjeta CHAR(16), nombre TEXT, apellido TEXT, domicilio TEXT, mes TEXT, anio TEXT) RETURNS VOID AS $$
DECLARE
	a_pagar INT; 
	aux RECORD;
	aux2 INT;
	nro_linea INT;
	desde DATE;
	hasta DATE;
	vence DATE; 
	v RECORD;
	terminacion_tarjeta INT;
	nombre_comercio TEXT;
	nro_resumen INT;
BEGIN
	-- Calcula si existen compras con la tarjeta --
	
	SELECT COUNT(*) FROM compra WHERE nrotarjeta = nro_tarjeta INTO aux2;
	IF aux2 = 0 THEN
	RETURN;
	ELSE

	-- Calcula el total a pagar --
	
	a_pagar := 0;

	FOR v IN SELECT * 
		FROM tarjeta t, compra c 
		WHERE t.nrotarjeta = c.nrotarjeta AND 
			t.nrocliente = nro_cliente AND 
			anio = TO_CHAR(c.fecha, 'YYYY') AND
			mes = TO_CHAR(c.fecha, 'MM')
	LOOP

		a_pagar = a_pagar + v.monto;

	END LOOP; 

	-- Cabecera (una por tarjeta) --

	terminacion_tarjeta := RIGHT(nro_tarjeta, 1);

	SELECT * 
	INTO aux 
	FROM cierre c 
	WHERE c.terminacion = terminacion_tarjeta;
	IF FOUND THEN
		desde := aux.fechainicio;
		hasta := aux.fechacierre;
		vence := aux.fechavto;
	END IF;

	INSERT INTO cabecera 
		VALUES(DEFAULT, nombre, apellido, domicilio, nro_tarjeta, desde, hasta, vence, a_pagar);
	
	-- Detalle (una por cada compra realizada) --
	
	nro_linea := 1;
	SELECT MAX(nroresumen) INTO nro_resumen FROM cabecera;

	FOR v IN SELECT * 
		FROM tarjeta t, compra c, comercio m 
		WHERE t.nrotarjeta = c.nrotarjeta AND 
			t.nrocliente = nro_cliente AND 
			c.nrocomercio = m.nrocomercio AND
			anio = TO_CHAR(c.fecha, 'YYYY') AND
			mes = TO_CHAR(c.fecha, 'MM')
	LOOP
		
		UPDATE compra SET pagado = TRUE WHERE nrooperacion = v.nrooperacion; 

		INSERT INTO detalle VALUES(nro_resumen, nro_linea, v.fecha, v.nombrecomercio, v.monto);
		nro_linea := nro_linea + 1;

	END LOOP;
	END IF;

END;
$$ LANGUAGE PLPGSQL; 

-- Alertas --

-- Se produce una alerta cuando se genera un rechazo
CREATE OR REPLACE FUNCTION alerta_0() RETURNS TRIGGER AS $$
DECLARE
BEGIN
	INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 0, 'Se ha producido un rechazo');

	RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER alerta_0_trigger AFTER INSERT ON rechazo
FOR EACH ROW EXECUTE PROCEDURE alerta_0();

-- Se produce una alerta cuando se registran dos compras con la misma tarjeta en un lapso menor de 1 minuto en comercios con el mismo código postal
CREATE OR REPLACE FUNCTION alerta_1() RETURNS TRIGGER AS $$
DECLARE
	ultima_compra INT;
	diferencia INT;
	nroalerta INT;
	ult_cod_postal CHAR(8);
	new_cod_postal CHAR(8);
BEGIN
	PERFORM
        FROM
        compra c                                                        
        INNER JOIN comercio m 
	ON m.nrocomercio = c.nrocomercio
        WHERE c.nrotarjeta = new.nrotarjeta AND
        c.nrocomercio != new.nrocomercio AND
	m.codigopostal = (SELECT codigopostal
                FROM comercio
                WHERE nrocomercio = new.nrocomercio) AND
        	EXTRACT(EPOCH FROM(new.fecha - c.fecha)) < 60;
        IF FOUND THEN
		INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, -1, 1, 'Se registraron dos compras con la misma tarjeta en un lapso menor a un minuto en comercios ubicados en el mismo código postal');
	END IF;

	RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER alerta_1_trigger BEFORE INSERT ON compra 
FOR EACH ROW EXECUTE PROCEDURE alerta_1();

-- Se produce una alerta cuando se registran dos compras con la misma tarjeta en un lapso menor de 5 minutos en comercios con distinto código postal
CREATE OR REPLACE FUNCTION alerta_5() RETURNS TRIGGER AS $$
DECLARE
	ultima_compra INT;
	diferencia INT;
	nroalerta INT;
	old_cod_postal CHAR(8);
	new_cod_postal CHAR(8);
BEGIN
	PERFORM
        FROM compra c
        INNER JOIN comercio m 
	ON m.nrocomercio = c.nrocomercio
        WHERE c.nrotarjeta = new.nrotarjeta AND 
	m.codigopostal != (SELECT codigopostal
        	FROM comercio
                WHERE nrocomercio = new.nrocomercio) AND 
		EXTRACT(EPOCH FROM(new.fecha - c.fecha)) < 300;
	IF FOUND THEN
		INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, -1, 5, 'Se registraron dos compras con la misma tarjeta en un lapso menor a cinco minutos en comercios ubicados en distinto código postal');
	END IF;

	RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER alerta_5_trigger BEFORE INSERT ON compra 
FOR EACH ROW EXECUTE PROCEDURE alerta_5();

-- Se produce una alerta si una tarjeta registra dos rechazos por exceso de límite en el mismo día, y la tarjeta se suspende
CREATE OR REPLACE FUNCTION alerta_32() RETURNS TRIGGER AS $$
DECLARE
	ultimo_rechazo INT;
	aux RECORD;
	diferencia INT;
BEGIN
	IF new.motivo::TEXT = 'Supera límite de tarjeta'::TEXT THEN

		SELECT MAX(nrorechazo) INTO ultimo_rechazo FROM rechazo;

		SELECT * 
		INTO aux 
		FROM rechazo 
		WHERE nrorechazo = ultimo_rechazo AND 
			motivo = new.motivo;

		diferencia := EXTRACT(EPOCH FROM(new.fecha - aux.fecha));

		IF diferencia < 86400 THEN --Argh

			UPDATE tarjeta SET estado = 'suspendida' WHERE nrotarjeta = new.nrotarjeta;

			INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Se registraron dos rechazos por exceso de límite en el mismo día. La tarjeta fue suspendida preventivamente.');
		END IF;
	
	END IF;

	RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER alerta_32_trigger AFTER INSERT ON rechazo 
FOR EACH ROW EXECUTE PROCEDURE alerta_32();
