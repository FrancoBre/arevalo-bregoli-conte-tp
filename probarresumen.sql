\c tarjetadecredito;

CREATE OR REPLACE FUNCTION probar_resumen() RETURNS VOID AS $$ 
DECLARE
	nro_cliente INT;
	v RECORD;
	aux RECORD;
	mes TEXT;
	anio TEXT;
BEGIN
	
	FOR v IN SELECT * FROM cliente WHERE nrocliente = 1 
	LOOP
		nro_cliente := v.nrocliente;
		mes := TO_CHAR(CURRENT_TIMESTAMP, 'MM');
		anio := TO_CHAR(CURRENT_TIMESTAMP, 'YYYY');

		SELECT resumen_cliente(nro_cliente, mes, anio) INTO aux;

       END LOOP;

END;
$$ LANGUAGE PLPGSQL;
