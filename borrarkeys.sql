\c tarjetadecredito

ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_nrocliente_fk;
ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_nrotarjeta_fk;
ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_nrocomercio_fk;
ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_nrotarjeta_fk;
ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_nrocomercio_fk;
ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS cabecera_nrotarjeta_fk;
ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_nrotarjeta_fk;
ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_nrorechazo_fk;

ALTER TABLE cliente DROP CONSTRAINT IF EXISTS cliente_pk;
ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_pk;
ALTER TABLE comercio DROP CONSTRAINT IF EXISTS comercio_pk;
ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_pk;
ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_pk;
ALTER TABLE cierre DROP CONSTRAINT IF EXISTS cierre_pk;
ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS cabecera_pk;
ALTER TABLE detalle DROP CONSTRAINT IF EXISTS detalle_pk;
ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_pk;
